# prob_program_rstanarm_example

This is an R Markdown document for demonstrating a simple example of probabilistic programming in R
programming framework to perform GLM using the rstanarm package.

Adapted from:

    * https://mc-stan.org/users/documentation/case-studies/tutorial_rstanarm.html

    * http://mc-stan.org/bayesplot/articles/plotting-mcmc-draws.html
